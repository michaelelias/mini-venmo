package venmo;

import venmo.command.Command;
import venmo.command.CommandParser;
import venmo.model.VenmoModel;
import venmo.util.ValidationErrorType;
import venmo.util.ValidationException;

import java.io.*;

/**
 * The main class for the MiniVenmoApp.
 * Supports both interactive and batch mode:
 *  Interactive : start the app with no arguments and a commandline is presented where the user can type in commands
 *  Batch       : when an argument is provided, it is assumed it is a file name and read. Each line in the file is processed as a command and replayed to the commandline
 */
public class MiniVenmoApp
{
    private static MiniVenmoApp instance;

    private VenmoModel model;
    private CommandParser commandParser;

    MiniVenmoApp(){
        model = new VenmoModel();
        commandParser = new CommandParser();
    }

    public static VenmoModel getModel(){
        return instance.model;
    }

    public static CommandParser getCommandParser() {
        return instance.commandParser;
    }

    public static void main( String[] args )
    {
        instance = new MiniVenmoApp();
        Console console = System.console();

        if(args.length > 1){
            console.printf(ValidationErrorType.ARGUMENTS_INVALID.getMessage());
            return;
        }

        console.printf("Welcome to Venmo Mini!");

        if(args.length == 1){
            startBatchMode(console, args[0]);
        } else {
            startInteractiveMode(console);
        }
    }

    private static void startInteractiveMode(Console console){
        do {
            String line = console.readLine("\n>");
            processLine(console, line);
        } while(true);
    }

    private static void startBatchMode(Console console, String fileName){
        console.printf("\nStarting batch mode");
        File file = new File(fileName);
        if(!file.exists()){
            console.printf("\nFile does not exist");
            return;
        }
        if(!file.canRead()){
            console.printf("\nCannot read from file. Check permissions");
            return;
        }
        if(!file.isFile()){
            console.printf("\nNot a file");
            return;
        }
        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String line;
            while((line = reader.readLine()) != null){
                console.printf("\n>" + line);
                processLine(console, line);
            }
        } catch (FileNotFoundException e) {
            console.printf("\nFile not found");
            return;
        } catch (IOException e) {
            console.printf("\nError reading file");
            return;
        }
    }

    private static void processLine(Console console, String line){
        try {
            Command command = getCommandParser().parse(line.split(" "));
            String response = command.execute();
            if(response != null){
                String[] linesToPrint = response.split("\n");
                for(String lineToPrint : linesToPrint){
                    console.printf("\n-- " + lineToPrint);
                }
            }
        } catch(ValidationException e){
            console.printf("\nERROR: " + e.getMessage());
        }
    }

}
