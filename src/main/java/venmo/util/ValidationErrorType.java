package venmo.util;

public enum ValidationErrorType {

    ARGUMENTS_INVALID("Arguments invalid"),
    COMMAND_ARGUMENTS_INVALID("Invalid arguments for that command"),
    COMMAND_INVALID("That command is invalid"),
    USERNAME_TOO_SHORT("Username must be atleast 4 characters long"),
    USERNAME_TOO_LONG("Username cannot be longer than 15 characters"),
    USERNAME_INVALID_CHARS("Username can only contain alphanumeric characters, underscores or dashes"),
    USERNAME_TAKEN("Username %s is already taken"),

    USER_DOESNT_EXIST("No user with that name exists"),
    USER_ALREADY_LINKED_CARD("This user already has a valid credit card"),
    CARD_INVALID("This card is invalid"),
    CARD_ALREADY_LINKED_TO_OTHER_USER("That card has already been added by another user, reported for fraud!"),
    INVALID_AMOUNT("Invalid amount"),
    USER_DOESNT_HAVE_CARD("This user does not have a credit card"),
    CANNOT_PAY_YOURSELF("Cannot pay yourself");

    private String message;

    ValidationErrorType(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
