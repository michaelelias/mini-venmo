package venmo.util;

import java.util.regex.Pattern;

/**
 * Validates a String representation of a Dollar (USD) amount
 */
public class AmountValidator {

    public static final Pattern AMOUNT_PATTERN = Pattern.compile("\\$\\d+(\\.\\d{1,2})?");

    public static boolean validate(String amount){
        if(amount.length() == 0) {
            return false;
        }
        if(!AMOUNT_PATTERN.matcher(amount).matches()){
            return false;
        }
        return true;
    }

}
