package venmo.util;

/**
 * Validates a card number using Luhn-10 checksum
 */
public class CardNumberValidator {

    public static boolean validate(String cardNumber){
        if(cardNumber.length() == 0 || cardNumber.length() > 19){
            return false;
        }

        int sum = 0;
        boolean isAlternate = false;
        for (int i = cardNumber.length() - 1; i >= 0; i--) {
            int number = Integer.parseInt(cardNumber.substring(i, i + 1));
            if (isAlternate){
                number *= 2;
                if (number > 9){
                    number = (number % 10) + 1;
                }
            }
            sum += number;
            isAlternate = !isAlternate;
        }
        return (sum % 10 == 0);

    }

}
