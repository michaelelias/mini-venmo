package venmo.util;

public class ValidationException extends RuntimeException {

    private ValidationErrorType errorType;

    public ValidationException(ValidationErrorType errorType) {
        super(errorType.getMessage());
        this.errorType = errorType;
    }

    public ValidationErrorType getErrorType() {
        return errorType;
    }
}
