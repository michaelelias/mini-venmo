package venmo.command;

import venmo.util.CardNumberValidator;
import venmo.model.VenmoModel;
import venmo.util.ValidationErrorType;
import venmo.util.ValidationException;

public class AddCardCommand extends Command {

    protected AddCardCommand(VenmoModel model, String[] arguments) {
        super(model, arguments);
    }

    protected AddCardCommand(VenmoModel model, String user, String cardnumber){
        this(model, new String[]{user, cardnumber});
    }

    @Override
    public String execute() throws ValidationException {
        if(arguments.length != 2){
            throw new ValidationException(ValidationErrorType.COMMAND_ARGUMENTS_INVALID);
        }
        String username = arguments[0];
        String cardNumber = arguments[1];
        if(!model.doesUserExist(username)){
            throw new ValidationException(ValidationErrorType.USER_DOESNT_EXIST);
        }
        if(!CardNumberValidator.validate(cardNumber)){
            throw new ValidationException(ValidationErrorType.CARD_INVALID);
        }
        if(model.doesUserHaveCard(username)){
            throw new ValidationException(ValidationErrorType.USER_ALREADY_LINKED_CARD);
        }
        if(model.isCardAlreadyLinkedToOtherUserThan(username, cardNumber)){
            throw new ValidationException(ValidationErrorType.CARD_ALREADY_LINKED_TO_OTHER_USER);
        }

        model.addCardToUser(username, cardNumber);
        return null;
    }

}
