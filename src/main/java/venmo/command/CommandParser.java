package venmo.command;

import venmo.MiniVenmoApp;
import venmo.model.VenmoModel;
import venmo.util.ValidationErrorType;
import venmo.util.ValidationException;

import java.util.Arrays;

/**
 * A parser that takes in an array of string arguments and extract a valid command out of them, or throws an invalid command exception.
 */
public class CommandParser {

    public Command parse(String[] arguments){
        if(arguments.length <= 0){
            throw new ValidationException(ValidationErrorType.ARGUMENTS_INVALID);
        }
        String commandIdentifier = arguments[0];
        CommandType commandType = CommandType.resolve(commandIdentifier);
        if(commandType == null){
            throw new ValidationException(ValidationErrorType.COMMAND_INVALID);
        }
        String[] commandArguments = Arrays.copyOfRange(arguments, 1, arguments.length);
        return commandType.createCommand(MiniVenmoApp.getModel(), commandArguments);
    }

    public enum CommandType {

        ADD_USER("user") {
            @Override
            public Command createCommand(VenmoModel model, String[] arguments) {
                return new AddUserCommand(MiniVenmoApp.getModel(), arguments);
            }
        },
        ADD_CARD("add") {
            @Override
            public Command createCommand(VenmoModel model, String[] arguments) {
                return new AddCardCommand(MiniVenmoApp.getModel(), arguments);
            }
        },
        PAY("pay") {
            @Override
            public Command createCommand(VenmoModel model, String[] arguments) {
                return new PayCommand(MiniVenmoApp.getModel(), arguments);
            }
        },
        FEED("feed") {
            @Override
            public Command createCommand(VenmoModel model, String[] arguments) {
                return new FeedCommand(MiniVenmoApp.getModel(), arguments);
            }
        },
        BALANCE("balance") {
            @Override
            public Command createCommand(VenmoModel model, String[] arguments) {
                return new BalanceCommand(MiniVenmoApp.getModel(), arguments);
            }
        };

        private String identifier;

        CommandType(String identifier) {
            this.identifier = identifier;
        }

        public abstract Command createCommand(VenmoModel model, String[] arguments);

        public static CommandType resolve(String identifier){
            for(CommandType type : values()){
                if(type.identifier.equals(identifier)){
                    return type;
                }
            }
            return null;
        }
    }

}
