package venmo.command;

import venmo.model.VenmoModel;
import venmo.util.ValidationErrorType;
import venmo.util.ValidationException;

import java.util.regex.Pattern;

public class AddUserCommand extends Command {

    private static final Pattern ALLOWED_CHARACTERS_PATTERN = Pattern.compile("^[a-zA-Z0-9_-]*$");

    protected AddUserCommand(VenmoModel repo, String[] arguments) {
        super(repo, arguments);
    }

    protected AddUserCommand(VenmoModel repo, String username) {
        this(repo, new String[]{username});
    }

    @Override
    public String execute() throws ValidationException {
        if(arguments.length != 1){
            throw new ValidationException(ValidationErrorType.COMMAND_ARGUMENTS_INVALID);
        }
        String newUser = arguments[0];
        if(newUser.length() < 4){
            throw new ValidationException(ValidationErrorType.USERNAME_TOO_SHORT);
        }
        if(newUser.length() > 15){
            throw new ValidationException(ValidationErrorType.USERNAME_TOO_LONG);
        }
        if(!ALLOWED_CHARACTERS_PATTERN.matcher(newUser).matches()){
            throw new ValidationException(ValidationErrorType.USERNAME_INVALID_CHARS);
        }
        model.addUser(newUser);
        return null;
    }


}
