package venmo.command;

import venmo.model.VenmoModel;
import venmo.util.AmountValidator;
import venmo.util.ValidationErrorType;
import venmo.util.ValidationException;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

public class PayCommand extends Command {

    protected PayCommand(VenmoModel model, String[] arguments) {
        super(model, arguments);
    }

    protected PayCommand(VenmoModel model, String actorUsername, String targetUsername, String amount, String note){
        this(model, new String[]{actorUsername, targetUsername, amount, note});
    }

    @Override
    public String execute() throws ValidationException {
        if(arguments.length < 4){
            throw new ValidationException(ValidationErrorType.COMMAND_ARGUMENTS_INVALID);
        }

        String actorUsername = arguments[0];
        String targetUsername = arguments[1];
        String amount = arguments[2];
        String note = "";
        for(int i = 3; i < arguments.length; i++){
            note += arguments[i];
            if(i < arguments.length - 1){
                note += " ";
            }
        }

        if(!model.doesUserExist(actorUsername)){
            throw new ValidationException(ValidationErrorType.USER_DOESNT_EXIST);
        }
        if(!model.doesUserExist(targetUsername)){
            throw new ValidationException(ValidationErrorType.USER_DOESNT_EXIST);
        }
        if(actorUsername.equals(targetUsername)){
            throw new ValidationException(ValidationErrorType.CANNOT_PAY_YOURSELF);
        }
        if(!AmountValidator.validate(amount)){
            throw new ValidationException(ValidationErrorType.INVALID_AMOUNT);
        }
        if(!model.doesUserHaveCard(actorUsername)){
            throw new ValidationException(ValidationErrorType.USER_DOESNT_HAVE_CARD);
        }

        try {
            Number amountAsNumber = NumberFormat.getCurrencyInstance(Locale.US).parse(amount);
            model.pay(actorUsername, targetUsername, amountAsNumber.doubleValue(), note);
        } catch (ParseException e) {
            throw new ValidationException(ValidationErrorType.INVALID_AMOUNT);
        }
        return null;
    }


}
