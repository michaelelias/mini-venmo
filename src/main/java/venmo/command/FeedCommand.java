package venmo.command;

import venmo.model.Transaction;
import venmo.model.VenmoModel;
import venmo.util.ValidationErrorType;
import venmo.util.ValidationException;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

/**
 * Returns a feed of the transaction history of a specific user.
 * The feed is ordered chronologically.
 */
public class FeedCommand extends Command {

    private static final String FEED_TEMPLATE = "%s paid %s %s for %s";

    protected FeedCommand(VenmoModel model, String[] arguments) {
        super(model, arguments);
    }

    protected FeedCommand(VenmoModel model, String username) {
        super(model, new String[]{username});
    }


    @Override
    public String execute() throws ValidationException {
        if(arguments.length != 1){
            throw new ValidationException(ValidationErrorType.COMMAND_ARGUMENTS_INVALID);
        }

        String username = arguments[0];

        if(!model.doesUserExist(username)){
            throw new ValidationException(ValidationErrorType.USER_DOESNT_EXIST);
        }

        List<Transaction> transactions = model.listUserTransactionHistory(username);
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < transactions.size() ; i++){
            Transaction trx = transactions.get(i);
            String actor = trx.actor.equals(username) ? "You" : trx.actor;
            String target = trx.target.equals(username) ? "you" : trx.target;
            String amount = NumberFormat.getCurrencyInstance(Locale.US).format(trx.amount);
            String note = trx.note;
            sb.append(String.format(FEED_TEMPLATE, actor, target, amount, note));
            if(i < transactions.size() - 1){
                sb.append("\n");
            }
        }
        return sb.toString();
    }
}
