package venmo.command;

import venmo.model.VenmoModel;
import venmo.util.ValidationException;

/**
 * A base class that actual commands have to extend in order to work within the app.
 */
public abstract class Command {

    protected VenmoModel model;
    protected String[] arguments;

    protected Command(VenmoModel model, String[] arguments) {
        this.model = model;
        this.arguments = arguments;
    }

    /**
     * Executes the command.
     *
     * @return Returns the outcome of the command. This can be null or a success message.
     * @throws venmo.util.ValidationException When the command's arguments are invalid or other validation failed
     */
    public abstract String execute() throws ValidationException;

}
