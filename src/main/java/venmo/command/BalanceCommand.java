package venmo.command;

import venmo.model.VenmoModel;
import venmo.util.ValidationErrorType;
import venmo.util.ValidationException;

import java.text.NumberFormat;
import java.util.Locale;

public class BalanceCommand extends Command {

    protected BalanceCommand(VenmoModel model, String[] arguments) {
        super(model, arguments);
    }

    protected BalanceCommand(VenmoModel model, String username) {
        this(model, new String[]{username});
    }

    @Override
    public String execute() throws ValidationException {
        if(arguments.length != 1){
            throw new ValidationException(ValidationErrorType.COMMAND_ARGUMENTS_INVALID);
        }
        String username = arguments[0];
        if(!model.doesUserExist(username)){
            throw new ValidationException(ValidationErrorType.USER_DOESNT_EXIST);
        }

        Double balance = model.getBalance(username);
        return NumberFormat.getCurrencyInstance(Locale.US).format(balance);
    }
}
