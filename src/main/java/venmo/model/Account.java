package venmo.model;

import java.util.Set;
import java.util.TreeSet;

public class Account {

    /**
     * User that is the holder of this account
     */
    private User holder;

    /**
     * The current balance of the account;
     */
    private Double balance = 0.0;

    /**
     * A Set of Transaction ID's this account was involved in.
     */
    private Set<String> transactionLedger = new TreeSet<String>();

    /**
     * The Card associated with this account;
     */
    public Card card;

    public User getHolder() {
        return holder;
    }

    public void setHolder(User holder) {
        this.holder = holder;
    }

    public Double getBalance() {
        return balance;
    }

    public Set<String> getTransactionLedger() {
        return transactionLedger;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    /**
     * Registers a transaction with the account if the account is involved in the transaction.
     * If the account is the target of the transaction, the balance is incremented by the transaction's amount.
     * @param trx
     */
    public void registerTransaction(Transaction trx) {
        if(trx.actor.equals(holder.getUsername()) || trx.target.equals(holder.getUsername())){
            transactionLedger.add(trx.id);
            if(trx.target.equals(holder.getUsername())){
                balance += trx.amount;
            }
        }
    }
}
