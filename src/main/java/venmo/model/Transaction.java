package venmo.model;

import java.util.UUID;

/**
 * A record of an executed pay command.
 */
public class Transaction {

    public final String id;
    public Long timestamp;
    public String actor;
    public String target;
    public Double amount;
    public String note;

    public Transaction() {
        id = UUID.randomUUID().toString();
    }
}
