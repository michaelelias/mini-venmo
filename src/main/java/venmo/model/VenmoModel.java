package venmo.model;

import java.util.*;

/**
 * The model for the mini venmo app.
 * Uses several data structures to store the state of the app.
 */
public class VenmoModel {

    private Set<User> users = new HashSet<User>();
    private Set<Account> accounts = new HashSet<Account>();
    private Map<String, Transaction> transactionsById = new HashMap<String, Transaction>();

    public void addUser(String username){
        User user = new User(username);
        users.add(user);
        Account account = new Account();
        account.setHolder(user);
        accounts.add(account);
    }

    public boolean doesUserExist(String username){
        for(User user : users){
            if(user.getUsername().equals(username)){
                return true;
            }
        }
        return false;
    }

    public boolean doesUserHaveCard(String username){
        for(Account account : accounts){
            if(account.getHolder().getUsername().equals(username)){
                return account.getCard() != null;
            }
        }
        return false;
    }

    public boolean doesUserOwnCard(String username, String cardNumber){
        for(Account account : accounts){
            if(account.getHolder().getUsername().equals(username)){
                Card card = account.getCard();
                if(card != null){
                    return card.getNumber().equals(cardNumber);
                }
            }
        }
        return false;
    }

    public void addCardToUser(String username, String cardNumber){
        for(Account account : accounts){
            if(account.getHolder().getUsername().equals(username)){
                account.setCard(new Card(cardNumber));
                return;
            }
        }
    }

    public Double getBalance(String username){
        for(Account account : accounts){
            if(account.getHolder().getUsername().equals(username)){
                return account.getBalance();
            }
        }
        return 0.0;
    }

    public boolean isCardAlreadyLinkedToOtherUserThan(String username, String cardNumber) {
        for(Account account : accounts){
            Card card = account.getCard();
            if(card != null && card.getNumber().equals(cardNumber)){
                return !account.getHolder().getUsername().equals(username);
            }
        }
        return false;
    }

    public void pay(String actorUsername, String targetUsername, Double amount, String note) {
        Transaction trx = new Transaction();
        trx.actor = actorUsername;
        trx.target = targetUsername;
        trx.amount = amount;
        trx.note = note;
        trx.timestamp = System.currentTimeMillis();

        Account actorAccount = getAccountByUsername(actorUsername);
        Account targetAccount = getAccountByUsername(targetUsername);

        if(actorAccount != null && targetAccount != null){
            transactionsById.put(trx.id, trx);
            actorAccount.registerTransaction(trx);
            targetAccount.registerTransaction(trx);
        }

    }

    public List<Transaction> listUserTransactionHistory(String username){
        List<Transaction> trxs = new ArrayList<Transaction>();
        Account account = getAccountByUsername(username);
        for(String trxId : account.getTransactionLedger()){
            trxs.add(transactionsById.get(trxId));
        }
        return trxs;
    }

    private Account getAccountByUsername(String username){
        for(Account account : accounts){
            if(account.getHolder().getUsername().equals(username)){
                return account;
            }
        }
        return null;
    }
}
