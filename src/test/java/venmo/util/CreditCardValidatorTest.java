package venmo.util;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class CreditCardValidatorTest {

    @Test
    public void testValidNumber(){
        assertThat(CardNumberValidator.validate(ValidCardNumbers.CARD_ONE), is(true));
        assertThat(CardNumberValidator.validate(ValidCardNumbers.CARD_TWO), is(true));
        assertThat(CardNumberValidator.validate(ValidCardNumbers.CARD_THREE), is(true));
    }

    @Test
    public void testInvalidNumber(){
        assertThat(CardNumberValidator.validate(InvalidCardNumbers.CARD_ONE), is(false));
        assertThat(CardNumberValidator.validate(InvalidCardNumbers.CARD_TWO), is(false));
    }
}
