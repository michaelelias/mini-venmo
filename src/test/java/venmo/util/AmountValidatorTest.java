package venmo.util;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class AmountValidatorTest {

    @Test
    public void testValidAmounts(){
        assertThat(AmountValidator.validate("$0.10"), is(true));
        assertThat(AmountValidator.validate("$1.10"), is(true));
        assertThat(AmountValidator.validate("$10.10"), is(true));
        assertThat(AmountValidator.validate("$100.00"), is(true));
        assertThat(AmountValidator.validate("$100"), is(true));
        assertThat(AmountValidator.validate("$14"), is(true));
    }

    @Test
    public void testInvalidAmounts(){
        assertThat(AmountValidator.validate("$x.10"), is(false));
        assertThat(AmountValidator.validate("$1.100"), is(false));
        assertThat(AmountValidator.validate("$10.1x"), is(false));
        assertThat(AmountValidator.validate("%100.00"), is(false));
        assertThat(AmountValidator.validate("$100."), is(false));
        assertThat(AmountValidator.validate("$"), is(false));
    }

}
