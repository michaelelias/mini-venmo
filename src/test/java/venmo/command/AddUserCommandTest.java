package venmo.command;

import org.junit.Before;
import org.junit.Test;
import venmo.model.VenmoModel;
import venmo.util.ValidationErrorType;
import venmo.util.ValidationException;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

public class AddUserCommandTest {

    private VenmoModel model;

    @Before
    public void setup(){
        model = new VenmoModel();
    }

    @Test
    public void testEmptyArgumentsExpectException(){
        AddUserCommand cmd = new AddUserCommand(model, new String[]{});
        try {
            cmd.execute();
        } catch(ValidationException e){
            assertThat(e.getErrorType(), is(ValidationErrorType.COMMAND_ARGUMENTS_INVALID));
        }
    }

    @Test
    public void testTooShortUsernameExpectException(){
        executeCommandAssertException("Bob", ValidationErrorType.USERNAME_TOO_SHORT);
    }

    @Test
    public void testTooLongUsernameExpectException(){
        executeCommandAssertException("Bobinestiquevinota", ValidationErrorType.USERNAME_TOO_LONG);
    }

    @Test
    public void testInvalidCharactersExpectException(){
        executeCommandAssertException("Xe*oz12", ValidationErrorType.USERNAME_INVALID_CHARS);
    }

    @Test
    public void testValidNames(){
        executeCommandAssertUserAdded("MIKE");
        executeCommandAssertUserAdded("tango");
        executeCommandAssertUserAdded("Michaelangelo");
        executeCommandAssertUserAdded("aBbA_is-great");
    }

    private void executeCommandAssertException(String username, ValidationErrorType expectedErrorType){
        AddUserCommand cmd = new AddUserCommand(model, new String[]{username});
        assertThat(model.doesUserExist(username), is(false));
        try {
            cmd.execute();
            fail("Expected the command to throw an exception");
        } catch(ValidationException e){
            assertThat(e.getErrorType(), is(expectedErrorType));
            assertThat(model.doesUserExist(username), is(false));
        }
    }

    private void executeCommandAssertUserAdded(String username){
        AddUserCommand cmd = new AddUserCommand(model, new String[]{username});
        assertThat(model.doesUserExist(username), is(false));
        String actualResult = cmd.execute();
        assertThat(model.doesUserExist(username), is(true));
    }

}
