package venmo.command;

import org.junit.Before;
import org.junit.Test;
import venmo.model.VenmoModel;
import venmo.util.ValidCardNumbers;
import venmo.util.ValidationErrorType;
import venmo.util.ValidationException;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

public class FeedCommandTest {

    private VenmoModel model;

    private final String USER_ONE = "Mike";
    private final String USER_TWO = "Alice";

    @Before
    public void setup(){
        model = new VenmoModel();
        new AddUserCommand(model, USER_ONE).execute();
        new AddUserCommand(model, USER_TWO).execute();
        new AddCardCommand(model, USER_ONE, ValidCardNumbers.CARD_ONE).execute();
    }

    @Test
    public void testFeed(){
        new PayCommand(model, USER_ONE, USER_TWO, "$2.00", "Milkshake").execute();
        new PayCommand(model, USER_ONE, USER_TWO, "$3.00", "Bagel").execute();

        String feed = new FeedCommand(model, USER_ONE).execute();
        String[] feedLines = feed.split("\n");
        assertThat(feedLines[0], is("You paid " + USER_TWO + " $2.00 for Milkshake"));
        assertThat(feedLines[1], is("You paid " + USER_TWO + " $3.00 for Bagel"));

        feed = new FeedCommand(model, USER_TWO).execute();
        feedLines = feed.split("\n");
        assertThat(feedLines[0], is(USER_ONE + " paid you $2.00 for Milkshake"));
        assertThat(feedLines[1], is(USER_ONE + " paid you $3.00 for Bagel"));
    }

    @Test
    public void testFeedWithNonExistingUserExpectException(){
        try {
            new FeedCommand(model, "SomeBloke").execute();
            fail("Expected exception because user doesn't exist");
        } catch(ValidationException e){
            assertThat(e.getErrorType(), is(ValidationErrorType.USER_DOESNT_EXIST));
        }
    }

}
