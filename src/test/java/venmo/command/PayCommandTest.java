package venmo.command;

import org.junit.Before;
import org.junit.Test;
import venmo.model.VenmoModel;
import venmo.util.ValidCardNumbers;
import venmo.util.ValidationErrorType;
import venmo.util.ValidationException;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

public class PayCommandTest {

    private VenmoModel model;

    private final String USER_ONE = "Mike";
    private final String USER_TWO = "Alice";

    @Before
    public void setup(){
        model = new VenmoModel();
        new AddUserCommand(model, USER_ONE).execute();
        new AddUserCommand(model, USER_TWO).execute();
    }

    @Test
    public void testPay(){
        addCardToUser(USER_ONE, ValidCardNumbers.CARD_ONE);

        assertThat(balance(USER_TWO), is("$0.00"));

        new PayCommand(model, USER_ONE, USER_TWO, "$2.50", "Test Payment").execute();

        assertThat(balance(USER_TWO), is("$2.50"));
    }

    @Test
    public void testPayWithNonExistingUsersExpectException(){
        assertThatPayThrowsException("SomeBloke", USER_TWO, "$2.50", "Test Payment", ValidationErrorType.USER_DOESNT_EXIST);
        assertThatPayThrowsException("SomeBloke", "SomeGuy", "$2.50", "Test Payment", ValidationErrorType.USER_DOESNT_EXIST);
        assertThatPayThrowsException(USER_ONE, "SomeGuy", "$2.50", "Test Payment", ValidationErrorType.USER_DOESNT_EXIST);
    }

    @Test
    public void testPayWithWhereActorDoesntHaveLinkedCardExpectException(){
        assertThatPayThrowsException(USER_ONE, USER_TWO, "$2.50", "Test Payment", ValidationErrorType.USER_DOESNT_HAVE_CARD);
    }

    @Test
    public void testPayWithInvalidAmountExpectException(){
        addCardToUser(USER_ONE, ValidCardNumbers.CARD_ONE);
        assertThatPayThrowsException(USER_ONE, USER_TWO, "-$2.50", "Test Payment", ValidationErrorType.INVALID_AMOUNT);
        assertThatPayThrowsException(USER_ONE, USER_TWO, "$a.50", "Test Payment", ValidationErrorType.INVALID_AMOUNT);
        assertThatPayThrowsException(USER_ONE, USER_TWO, "$2.50154", "Test Payment", ValidationErrorType.INVALID_AMOUNT);
    }

    @Test
    public void testPayWithSameActorAndTargetExpectException(){
        addCardToUser(USER_ONE, ValidCardNumbers.CARD_ONE);
        assertThatPayThrowsException(USER_ONE, USER_ONE, "$2.50", "Test Payment", ValidationErrorType.CANNOT_PAY_YOURSELF);

    }

    private void assertThatPayThrowsException(String actor, String target, String amount, String note, ValidationErrorType expectedErrorType){
        try {
            new PayCommand(model, actor, target, amount, note).execute();
            fail("Expected exception");
        } catch(ValidationException e){
            assertThat(e.getErrorType(), is(expectedErrorType));
        }
    }

    private void addCardToUser(String username, String cardNumber){
        new AddCardCommand(model, username, cardNumber).execute();
    }

    private String balance(String username){
        return new BalanceCommand(model, username).execute();
    }

}
