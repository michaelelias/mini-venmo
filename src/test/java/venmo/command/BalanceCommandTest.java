package venmo.command;

import org.junit.Before;
import org.junit.Test;
import venmo.model.VenmoModel;
import venmo.util.ValidationErrorType;
import venmo.util.ValidationException;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

public class BalanceCommandTest {

    private static final String USER = "Mike";

    private VenmoModel model;

    @Before
    public void setup(){
        model = new VenmoModel();
    }

    @Test
    public void testEmptyArguments(){
        BalanceCommand cmd = new BalanceCommand(model, new String[]{});
        try {
            cmd.execute();
            fail("Expected execute to throw an exception");
        } catch(ValidationException e){
            assertThat(e.getErrorType(), is(ValidationErrorType.COMMAND_ARGUMENTS_INVALID));
        }
    }

    @Test
    public void test(){
        model.addUser(USER);
        assertThat(new BalanceCommand(model, USER).execute(), is("$0.00"));
    }

    private void executeCommandAssertException(String username, ValidationErrorType expectedErrorType){
        BalanceCommand cmd = new BalanceCommand(model, new String[]{username});
        assertThat("Expected the user to exist", model.doesUserExist(username), is(true));
        try {
            cmd.execute();
            fail("Expected the command to throw an exception");
        } catch(ValidationException e){
            assertThat(e.getErrorType(), is(expectedErrorType));
            assertThat(model.doesUserExist(username), is(false));
        }
    }

}
