package venmo.command;

import org.junit.Before;
import org.junit.Test;
import venmo.model.VenmoModel;
import venmo.util.InvalidCardNumbers;
import venmo.util.ValidCardNumbers;
import venmo.util.ValidationErrorType;
import venmo.util.ValidationException;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

public class AddCardCommandTest {

    private VenmoModel model;

    private final String USER_ONE = "Mike";
    private final String USER_TWO = "Alice";

    @Before
    public void setup(){
        model = new VenmoModel();
        new AddUserCommand(model, USER_ONE).execute();
        new AddUserCommand(model, USER_TWO).execute();
    }

    @Test
    public void testAddCardToUser(){
        assertThat(model.doesUserHaveCard(USER_ONE), is(false));

        new AddCardCommand(model, USER_ONE, ValidCardNumbers.CARD_ONE).execute();

        assertThat(model.doesUserHaveCard(USER_ONE), is(true));
        assertThat(model.doesUserOwnCard(USER_ONE, ValidCardNumbers.CARD_ONE), is(true));
    }

    @Test
    public void testAddInvalidCardToUser(){
        assertThat(model.doesUserHaveCard(USER_ONE), is(false));

        try {
            new AddCardCommand(model, USER_ONE, InvalidCardNumbers.CARD_ONE).execute();
            fail("Expected exception because card has an invalid number");
        } catch(ValidationException e){
            assertThat(e.getErrorType(), is(ValidationErrorType.CARD_INVALID));
            assertThat(model.doesUserHaveCard(USER_ONE), is(false));
        }
    }

    @Test
    public void testAddAlreadyLinkedCardToOtherUser(){
        new AddCardCommand(model, USER_ONE, ValidCardNumbers.CARD_ONE).execute();

        assertThat(model.doesUserHaveCard(USER_ONE), is(true));
        assertThat(model.doesUserOwnCard(USER_ONE, ValidCardNumbers.CARD_ONE), is(true));
        assertThat(model.doesUserHaveCard(USER_TWO), is(false));
        assertThat(model.doesUserOwnCard(USER_TWO, ValidCardNumbers.CARD_ONE), is(false));

        try {
            new AddCardCommand(model, USER_TWO, ValidCardNumbers.CARD_ONE).execute();
            fail("Expected exception because card is already linked to other user");
        } catch (ValidationException e){
            assertThat(e.getErrorType(), is(ValidationErrorType.CARD_ALREADY_LINKED_TO_OTHER_USER));
        }

    }

}
