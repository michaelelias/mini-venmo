# Welcome to an implementation of the Mini Venmo App by @MichaelElias88

## Requirement : Java 6.0
 - type `java -version` on your CLI. If the version is > 1.6 you're golden
 - install on **nix : `sudo apt-get install openjdk-6-jdk`
 - install in windows : google for java 6 JDK, download and install
 - make sure to get the JDK not the JRE

## How to run
 - **nix : `./mini-venmo.sh` for interactive mode, `./mini-venmo.sh {batch-file}` for batch mode
 - windows : `mini-venmo.bat` for interactive mode, `mini-venmo.bat {batch-file}` for batch mode

## App Design Mantra
The core of the app is the VenmoModel which represents the state of the app. Commands are used to manipulate the model. Each command is responsible for it's validation and execution.

The model consists of :

 - User : represents a User, obviously
 - Account : an account holds the balance, card and transactions a user was involved in.
 - Card : holds a credit card's details
 - Transaction : a record of an executed pay command. Holds the actor, target, amount and note of a pay command. Also has a unique ID used to store a reference to the transaction in the involved Accounts.

It's easy to add new commands with the established pattern or to change the behaviour existing commands as all the logic for a specific command is contained in one command class.

## Why Java?
I'm very comfortable with Java and know it in an out. It's got a built in collection framework, CLI I/O, Currency formatting/parsing support, Regular Expressions. A simple OO paradigm and strong typing make it difficult to shoot ones own foot.

## Running tests
Tests live in src/test/java dir, they require Maven to run.

 - install Maven (**nix `sudo apt-get install maven` or download and install it on windows)
 - `mvn test` to run the tests






