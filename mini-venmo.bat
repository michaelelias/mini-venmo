@echo off
rem Script for compiling and starting the MiniVenmo app on Windows
RMDIR /S /Q target
MKDIR target\\classes
javac -cp src\\main\\java\\ -d target\\classes src\\main\\java\\venmo\\*.java
java -cp target\\classes venmo.MiniVenmoApp %1
@echo off
