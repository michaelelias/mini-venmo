﻿#!/bin/bash

rm -f -r target

mkdir -p target/classes

javac -cp src/main/java -d target/classes src/main/java/venmo/*.java

java -cp target/classes venmo.MiniVenmoApp $1

